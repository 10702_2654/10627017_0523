package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {


    public Button btnWorking;
    public ImageView iv;
    public ProgressBar bpWorking;

    public void strat(ActionEvent actionEvent) {
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                final int max = 100;
                bpWorking.setVisible(true);
                for (int i = 1; i <= max; i++) {
                   Thread.sleep(100);
                   updateProgress(i,max);
                }
                return null;
            }
            @Override
            protected void succeeded() {
                super.succeeded();
                bpWorking.setVisible(false);
            }

        };
        //bpWorking.setProgress(0.0);

        bpWorking.progressProperty().bind(task.progressProperty());

        new Thread(task).start();
        List<String> abc = new ArrayList<>();
        for(int i=0;i<5;i++){
            abc.add("/resources/".concat(String.valueOf(i)).concat(".png"));
        }
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        for (int i=0;i<5;i++){
            int j=i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource(abc.get(j)).toString()));
                        }
                    })
            );
        }timeline.play();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
